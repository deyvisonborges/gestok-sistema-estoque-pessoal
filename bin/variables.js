'use strict'

module.exports.variables = {
	port: '3000',
	database:  process.env.port || 'mongodb://127.0.0.1:27017'
}
