'use strict'

// definindo módulos
const ejs = require('ejs');
const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const variable = require('./variables');
const bodyParser = require('body-parser');

const app = express();

// estabelecendo conexao com o banco de dadoss
mongoose.connect('mongodb://127.0.0.1:27017');

// definindo template engine
app.set('view engine', 'ejs');
app.set('views', 'views');

// configurando parser e arquivos estáticos
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'))

// ao chamar endpoint raiz
app.get('/', (req, res, next) => {
    res.render('login');
});

module.exports = app;
